// alert("YOU'RE IN MASTER")

let userNumber = Number(prompt("Please enter a number"));
let divByTen = "Is divisible by 10, therefore, will be skipped."
let isPrinted = " is printed."

/* 	For loop that will countdown the number the user define. 
	Will print divByTen if the number on countdown is %10=0 and will skip counting.
	Will print the number and isPrinted string if number on countdown is %5=0.
	Will stop if the countdown number is <= 50.
*/
for(userNumber; userNumber >= 0; userNumber--) {
	// userNumber == userNumber - 1;
	if(userNumber%10 == 0){
		console.log(divByTen);
		continue;
	}
	if( userNumber <= 50){
		console.log("Countdown reached to 50. The loop will now stop.")
		break;
	}

	console.log(userNumber);

	if(userNumber%5 == 0){
		console.log("Number "+userNumber+" divisible by 5"+isPrinted);
	}
}

let longWord = "supercalifragilisticexpialidocious";
let consonant = "";

/*
	For loop that will collect every consonants in the longWord strind variable.
*/
for(let i = 0; i < longWord.length; i++) { 
	if( // if argument that will skip all the vowels and proceed to next iteration.
		longWord[i].toLowerCase() === 'a' ||
		longWord[i].toLowerCase() === 'e' ||
		longWord[i].toLowerCase() === 'i' ||
		longWord[i].toLowerCase() === 'o' ||
		longWord[i].toLowerCase() === 'u' 
	) {
		continue;
	} else {
		consonant += longWord[i]+", "; // This is where the consonants are being stored on the second string variable consonant.
	}
}
		console.log("These are the consonants of "+longWord+":")
		console.log(consonant);